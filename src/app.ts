import express from "express";
import log from "@ajar/marker";
import morgan from "morgan";
import usersRoute from "./routers/usersRoute.js";
import { errorsLogger } from "./loggers/errorLogers.js";
import { httpLoger } from "./loggers/httpLoggers.js";
import DB from "./dataBases/dbJson.js";
import { generateUuid } from "./middleFunctions/usersMiddle.js";
import { notFound } from "./html/notFound404.js";

function init() {
    const HTTP_LOG_FILE_PATH = "./logs/httpLogs.log";
    const ERRORS_LOG_FILE_PATH = "./logs/errorLogs.log";
    const { PORT, HOST } = initializeEnv();
    const { httpMiddleWareLogger, errorsMiddleWearLogger } = initializeLoggers(
        HTTP_LOG_FILE_PATH,
        ERRORS_LOG_FILE_PATH
    );
    const app = express();
    assignToApp(app,httpMiddleWareLogger,errorsMiddleWearLogger);
    serverListen(PORT, HOST, app);
}

function assignToApp(app:express.Application,httpLoger:any,errorsLogger:any){
        // Assign middleWare functions.
        app.use(morgan("dev"));
        app.use(generateUuid);
        app.use(httpLoger);
        app.use(express.json());
    
        // Users routing.
        app.use("/users", usersRoute);
    
        // Errors handling.
        app.use(errorsLogger);
        app.use((req: express.Request, res: express.Response) => {
            res.status(404).send(notFound);
        });

}
function initializeLoggers(httpLogFilePath: string, errosLogFilePath: string) {
    const httpMiddleWareLogger = httpLoger(httpLogFilePath);
    const errorsMiddleWearLogger = errorsLogger(errosLogFilePath);

    return { httpMiddleWareLogger, errorsMiddleWearLogger };
}
function initializeEnv() {
    return process.env;
}

function serverListen(
    PORT: string | undefined,
    HOST: string | undefined,
    app: express.Application
) {
    app.listen(Number(PORT), String(HOST), () => {
        log.magenta("🌎  listening on", `http://${HOST}:${PORT}`);
        DB.init();
    });
}

init();
