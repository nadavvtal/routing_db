import DB from "../dataBases/dbJson.js";
import { User } from "../types/usersTypes.js";
class UserModel {
    getAllUsers() {
        return DB.dataBase.users;
    }

    addNoteToArr(age: string, gender: string, description: string): void {
        const user: User = {
            id: DB.dataBase.currentId,
            gender: gender,
            age: age,
            description: description,
        };
        DB.dataBase.users.push(user);
        DB.counterUp();
        DB.writeToFile();
    }

    updateUserInArr(
        id: string,
        gender: string,
        age: string,
        description: string
    ): void {
        const userUpdate: User = {
            id: id,
            gender: gender,
            age: age,
            description: description,
        };

        DB.dataBase.users = DB.dataBase.users.map((user: User) => {
            if (user.id === Number(id)) {
                return userUpdate;
            }
            return user;
        });
    }

    getUser(id: string) {
        return DB.dataBase.users.find((user: any) => {
            return user.id === Number(id);
        });
    }

    deleteUser(id: string) {
        DB.dataBase.users = DB.dataBase.users.filter((user: any) => {
            return user.id !== Number(id);
        });
        DB.writeToFile();
    }
}
export const userModel = new UserModel();
