import fs from "fs/promises";
import log from "@ajar/marker";
import { DataBase } from "../types/usersTypes.js";
class DBmodel {
    path = "";
    encode = "utf-8";
    dataBase: DataBase = {
        users: [],
        currentId: 1,
    };
    constructor(db_json_path: string) {
        this.path = db_json_path;
    }

    async init() {
        try {
            const dbExist: void = await fs.access(this.path);
        } catch (err) {
            await fs.writeFile(this.path, "");
            log.green("New DB.json file created");
        }
        await this.reload();
    }

    async reload() {
        let DBjson: any = await fs.readFile(this.path, "utf-8");
        // Checks if data base is empty.
        if (!DBjson) {
            this.writeToFile();
        }
        DBjson = await fs.readFile(this.path, "utf-8");
        this.dataBase = JSON.parse(DBjson);
    }

    async writeToFile(): Promise<void> {
        try {
            await fs.writeFile(
                this.path,
                JSON.stringify(this.dataBase, null, 2)
            );
        } catch (error) {
            log.red("Error in writing to data base", error);
            throw new Error("Error");
        }
    }
    counterUp() {
        this.dataBase.currentId++;
    }
}

const db_json_users = new DBmodel("./DB_USERS.json");
export default db_json_users;
