import { Request, Response, NextFunction } from "express";
import DB from "../dataBases/dbJson.js";
import pkg from "uuid";
const { v4: uuid } = pkg;

export const idExist = function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const idExistInDB = DB.dataBase.users.some((user) => {
            return user.id === Number(req.params.id);
        });
        if (!idExistInDB) {
            throw new Error("No such id");
        }
        next();
    } catch (err) {
        next(err);
    }
};

export const generateUuid = function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const uniqIdentifier: string = uuid();
        req.uuid = uniqIdentifier;
        next();
    } catch (err) {
        next(err);
    }
};
