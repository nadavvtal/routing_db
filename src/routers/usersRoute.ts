import { Request, Response, NextFunction } from "express";
import express from "express";
import { idExist } from "../middleFunctions/usersMiddle.js";
import { userController } from "../controllers/userController.js";

const router = express.Router();
router.use(express.json());

// Get all users.
router.get("/", userController.getAllUsers);

// Add new user to database.
router.post("/", userController.addUser);

// Get specific user.
router.get("/:id", idExist, userController.getUser);

// Delete user.
router.delete("/:id", idExist, userController.deleteUser);

// Update user details.
router.put("/:id", idExist, userController.updateUser);

// Handle users route errors.
router.use((req: Request, res: Response, next: NextFunction) => {
    res.status(404).send("Are you sure you know our users?");
});

export default router;
