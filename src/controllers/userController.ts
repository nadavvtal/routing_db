import { userModel } from "../models/userModel.js";
import { Request, Response, NextFunction } from "express";
class UserController {
    addUser(req: Request, res: Response, next: NextFunction) {
        console.log("inpost", req.path);
        try {
            userModel.addNoteToArr(
                req.body.age,
                req.body.gender,
                req.body.description
            );
            res.status(200).send("user added to DB");
        } catch (err) {
            next(err);
        }
    }
    getAllUsers(req: Request, res: Response, next: NextFunction) {
        try {
            const response = userModel.getAllUsers();
            res.status(200).send(response);
        } catch (err) {
            next(err);
        }
    }

    getUser(req: Request, res: Response, next: NextFunction) {
        try {
            const response = userModel.getUser(req.params.id);
            res.status(200).send(response);
        } catch (err) {
            next(err);
        }
    }

    updateUser(req: Request, res: Response, next: NextFunction) {
        try {
            userModel.updateUserInArr(
                req.params.id,
                req.body.gender,
                req.body.age,
                req.body.description
            );
            res.status(200).send("user updated");
        } catch (err) {
            next(err);
        }
    }

    deleteUser(req: Request, res: Response, next: NextFunction) {
        try {
            userModel.deleteUser(req.params.id);
            res.status(200).send(`user ${req.params.id} deleted`);
        } catch (err) {
            next(err);
        }
    }
}

export const userController = new UserController();
